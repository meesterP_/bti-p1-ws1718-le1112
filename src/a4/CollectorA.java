package a4;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import thingy.Item;

public class CollectorA implements Collector_I {
    private List<Item> list;
    private List<Item> returnList;

    public CollectorA() {
        list = new ArrayList<Item>();
        returnList = new ArrayList<Item>();
    }

    @Override
    public Collection<Item> process(Item item) {
        list.add(item);
        if (list.size() >= 5) {
            returnList.clear();
            returnList.addAll(list);
            list.clear();
            return returnList;
        } else {
            return null;
        }
    }

    @Override
    public void reset() {
        list.clear();
        returnList.clear();
    }
}
