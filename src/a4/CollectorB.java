package a4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import thingy.Item;
import thingy.Size;

public class CollectorB implements Collector_I {
    private List<Item> listSmall;
    private List<Item> listMedium;
    private List<Item> listLarge;
    private List<Item> listToReturn;

    public CollectorB() {
        listSmall = new ArrayList<Item>();
        listMedium = new ArrayList<Item>();
        listLarge = new ArrayList<Item>();
        listToReturn = new ArrayList<Item>();
    }

    @Override
    public Collection<Item> process(Item item) {
        Size size = item.getSize();
        List<Item> currentList;

        // switch (size) {
        // case SMALL:
        // listSmall.add(item);
        // currentList = listSmall;
        // break;
        // case MEDIUM:
        // listMedium.add(item);
        // currentList = listMedium;
        // break;
        // case LARGE:
        // listLarge.add(item);
        // currentList = listLarge;
        // break;
        // default:
        // return null;
        // }
        if (size == Size.SMALL) {
            listSmall.add(item);
            currentList = listSmall;
        } else if (size == Size.MEDIUM) {
            listMedium.add(item);
            currentList = listMedium;
        } else {
            listLarge.add(item);
            currentList = listLarge;
        }
        if (currentList.size() >= 3) {
            listToReturn.clear();
            listToReturn.addAll(currentList);
            currentList.clear();
            return listToReturn;
        } else {
            return null;
        }
    }

    @Override
    public void reset() {
        listSmall.clear();
        listMedium.clear();
        listLarge.clear();
        listToReturn.clear();
    }

}
