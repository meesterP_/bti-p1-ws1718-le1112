package a4;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import thingy.Item;

public class CollectorC implements Collector_I {
    private List<Set<Item>> firstOrderList;

    public CollectorC() {
        firstOrderList = new ArrayList<Set<Item>>();
    }

    @Override
    public Collection<Item> process(Item item) {
        boolean isInSet = false;
        int firstOrderListPosition = 0;
        while (!isInSet && firstOrderListPosition < firstOrderList.size()) {
            if (firstOrderList.isEmpty()) {
                addSetToList();
                final Set<Item> currSet = firstOrderList.get(firstOrderListPosition);
                currSet.add(item);
                return null;
            } else {
                final Set<Item> currSet = firstOrderList.get(firstOrderListPosition);
                if (currSet.add(item)) {
                    isInSet = true;
                    if (currSet.size() >= 5) {
                        Collection<Item> result = currSet;
                        firstOrderList.remove(firstOrderListPosition);
                        return result;
                    } else {
                        return null;
                    }
                } else {
                    firstOrderListPosition++;
                }
            }
        }
        return null;
    }

    @Override
    public void reset() {
        firstOrderList.clear();
    }

    private void addSetToList() {
        Set<Item> newSet = new HashSet<Item>();
        firstOrderList.add(newSet);
    }

}
