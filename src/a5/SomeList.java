package a5;


/**
 * Eine bereits teilweise erfolgte Implementierung des LIST-Interface(s)
 */
public class SomeList /* implements List_I */ {
    
    // declaration(s)-----------------------------------------------------------
    
    public class NoDataAvailableException extends RuntimeException {
        private NoDataAvailableException( final String message ){
            super( message );
        }//constructor()
        private NoDataAvailableException(){
            this( "NO data available as result of list underflow" );
        }//constructor()
    }//class
    
    
    
    private  DoubleLinkNode head;                 // resp. first position  resp. front
    private  DoubleLinkNode tail;                 // resp. last position   resp. end
    
    
    
    
    
    // constructor(s)-----------------------------------------------------------
    
    /**
     * Der Konstruktor erzeugt eine leere Liste
     */
    public SomeList(){
        head = null;
        tail = null;
    }//constructor()
    
    
    
    
    
    // method(s)----------------------------------------------------------------
    
    /**
     * Prueft, ob zwei (list-)Objekte als gleich akzeptiert werden.
     * Dies ist genau dann der Fall, wenn die einzelnen Elemente gleich sind und
     * in jeweils beiden Listen in derselben Reihenfolge vorliegen.
     *
     * @return true if both (list-)objects are identical or contain equal equal elements in identical order
     */
    @Override
    public boolean equals( final Object otherObject ){
        if ( this == otherObject ) return true;
        if ( null == otherObject ) return false;
        if ( getClass() != otherObject.getClass() ) return false;
        //
        @SuppressWarnings("unchecked")
        final SomeList other = (SomeList)( otherObject );
        DoubleLinkNode work = head;
        DoubleLinkNode otherWork = other.head;
        while( null != work  &&  null != otherWork ){
            if( isUnequal( work.info, otherWork.info ))  return false;
            work = work.next;
            otherWork = otherWork.next;
        }//while
        return  work == otherWork;      // both have to be null now
    }//method()
    //
    private static boolean isUnequal( final Object o1,  final Object o2 ){
        return (o1!=o2) && ( (null==o1) || ( ! o1.equals( o2 )));
    }//method()
    
    
  //@Override
    public void clear(){
        head = null;
        tail = null;
    }//method()
    
    
  //@Override
    public boolean isEmpty(){
        assert ((null==head) == (null==tail)) :  "isEmpty-state inconsistent ;  was detected in \"[SomeList].isEmptpy()\"";
        return  null == head;
    }//method()
    
    
  //@Override
    public void fput( final Data data ){
        final DoubleLinkNode newNode = new DoubleLinkNode( data );
        newNode.next = head;
        if( null != head )  head.prev = newNode;
        if( null == tail )  tail = newNode;
        head = newNode;
    }//method()
    
  //@Override
    public void lput( final Data data ){
        final DoubleLinkNode newNode = new DoubleLinkNode( data );
        newNode.prev = tail;
        if( null != tail )  tail.next = newNode;
        if( null == head )  head = newNode;
        tail = newNode;
    }//method()
    
    
  //@Override
    public Data fget(){
        if( null == head ){
            throw new NoDataAvailableException();
        }else{
            final Data result = head.info;
            head = head.next;
            if( null != head ){
                head.prev = null;
            }else{
                tail = null;
            }//if
            return result;
        }//if
    }//method()
    
  //@Override
    public Data lget(){
        if( null == tail ){
            throw new NoDataAvailableException();
        }else{
            final Data result = tail.info;
            tail = tail.prev;
            if( null != tail ){
                tail.next = null;
            }else{
                head = null;
            }//if
            return result;
        }//if
    }//method()
    
    
    
    /**
     * toString delivers only "subset" : prev- & next-pointer are missing(!)
     *
     * @return string representation of (list-)object without prev- and next- pointer
     */
    @Override
    public String toString(){
        final StringBuffer sb = new StringBuffer();
        DoubleLinkNode work = head;
        if( null==work ){
            sb.append( "null" );
        }else{
            loop:
            while(true){
                sb.append( work.info.toString() );
                if( tail==work )  break loop;
                sb.append( ", " );
                work = work.next;
            }//while
        }//if
        return String.format(
            "[<%s> %s]",
            SomeList.class.getSimpleName(),
            sb.toString()
        );
    }//method()
    
}//interface
