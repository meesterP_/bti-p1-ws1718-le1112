package a5;


/**
 * LabExam1112_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Das Interface List_I
 * <ul>
 *     <li>beschreibt eine Liste und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und
 *         fordert die entsprechenden Methoden ein.
 *     </li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse List muss
 * <ul>
 *     <li>einen oeffentlichen parameterlosen Konstruktor aufweisen.
 *     </li>
 *     <li>eine selbstgeschriebene dynamische Datenstruktur sein.
 *         Sie duerfen fuer die Implementierung von List insbesondere NICHT auf die Collections zurueckgreifen.
 *         Die Implementierung muss in Form einer verketteten Liste erfolgen.
 *         Sie sollen die mitausgelieferten Klassen Data fuer Ihre Implementierung nutzen.
 *         Optional koennen Sie auf die mitausgelieferten Klassen SomeList und DoubleLinkNode
 *         nach belieben zurueckgreifen.
 *      </li>
 * </ul>
 * Bemerkung:<br />
 * Noch einmal: Fuer diese Aufgabe duerfen Sie NICHT auf die Collections zurueckgreifen.
 * Sie muessen die Liste mit <strong>eigenen Mitteln</strong> selbst implementieren
 * und zwar als <strong>dynamische Datenstruktur</strong> bzw. konkret als verkettete Liste.<br />
 * Um Sie von Generics zu entlasten wurde auf die Klasse Data zurueckgegriffen,
 * die Sie auch schon aus der Programmieren-Vorlesung als Platzhalter-Klasse kennen
 * (und die hier die Rolle des Generics einnimmt).<br />
 * Sofern Sie auf die Klasse SomeList zurueckgreifen, muessen Sie nur die Methode reverse() implementieren.
 *<br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse
 * findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/Prg_P1_LE_s17s_WI1_Proto.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_170710_v01
 */
public interface List_I {
    
    /**
     * Diese Methode leert die Liste.
     */
    void clear();
    
    
    
    /**
     * Die Operation fput() reiht einen Datensatz (data) am Anfang der Liste ein.
     * Falls der uebergebene Parameter keine gueltige Referenz enthaelt, ist geeignet zu reagieren.
     *<br />
     * @param data bestimmt den Datensatz, der in die Queue eingereiht werden soll.
     */
    void fput( Data data );
    
    /**
     * Die Operation lput() reiht einen Datensatz (data) am Ende der Liste ein.
     * Falls der uebergebene Parameter keine gueltige Referenz enthaelt, ist geeignet zu reagieren.
     *<br />
     * @param data bestimmt den Datensatz, der in die Queue eingereiht werden soll.
     */
    void lput( Data data );
    
    
    
    /**
     * Die Operation fget() liefert den Datensatz am Anfang der Liste und entfernt ihn aus der Liste.
     * Wenn die Liste leer ist, ist geeignet zu reagieren.
     *<br />
     * @return der Datensatz am Anfang der Liste.
     */
    Data fget();
    
    /**
     * Die Operation lget() liefert den Datensatz am Ende der Liste und entfernt ihn aus der Liste.
     * Wenn die Liste leer ist, ist geeignet zu reagieren.
     *<br />
     * @return der Datensatz am Ende der Liste.
     */
    Data lget();
    
    
    
    /**
     * Die Operation isEmpty() prueft, ob die Liste leer ist.
     *<br />
     * @return <code>true</code> falls die Liste leer ist und
     *         sonst <code>false</code>.
     */
    boolean isEmpty();
    
    
    
    /**
     * Die Operation reverse() spiegelt die Liste.
     *<br />
     * D.h. wenn die Liste n Elemente enthaelt, die jeweils an den Positionen 0 bis n-1 liegen,
     * dass ein Datensatz,
     * der vor dem Spiegeln an der Position p war,
     * nach dem Spiegeln an der Position n-1-p ist.
     */
    void reverse();
    
}//interface
