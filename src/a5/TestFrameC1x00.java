package a5;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
//port static org.junit.Assert.assertNotEquals;           // YATTB : BlueJ kann es nicht
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1112_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben.
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann nicht als Beweis dienen, dass der Code fehlerfrei ist.
 * Es liegt in Ihrer Verantwortung sicher zu stellen, dass Sie fehlerfreien Code geschrieben haben.
 * Bei der Bewertung werden andere - konkret : modifizierte und haertere Tests - verwendet.
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_170710_v10
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A / "1e"
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet, wird aber IMMER mit angestartet). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "List";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            final String testConstructorParameter = "test";                     // anything ;-)
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                TS.printDetailedInfoAboutObject( list, "list" );
                //
                if( TS.isActualMethod( list.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  list.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  list.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "List". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_List(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "List". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_List(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(                        classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isClassAccessModifierSet(       classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(                 classUnderTest, "List_I" ));
            //
            assertTrue( "requested constructor missing",    TS.isConstructor(                  classUnderTest ));
            assertTrue( "false constructor access modifier",TS.isConstructorAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            //
            if( TS.deepContainsForbiddenType( requestedRefTypeWithPath, new Class<?>[]{ Collection.class, Map.class, Data[].class } )){
                fail( "FORBIDDEN TYPE detected" );
            }//if
        }catch( final ClassNotFoundException | TestSupportException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "List" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_List(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing", TS.isMethod( classUnderTest, "clear",    void.class,    null ));
            assertTrue( "requested method missing", TS.isMethod( classUnderTest, "fput",     void.class,    new Class[]{ Data.class } ));
            assertTrue( "requested method missing", TS.isMethod( classUnderTest, "lput",     void.class,    new Class[]{ Data.class } ));
            assertTrue( "requested method missing", TS.isMethod( classUnderTest, "fget",     Data.class,    null ));
            assertTrue( "requested method missing", TS.isMethod( classUnderTest, "lget",     Data.class,    null ));
            assertTrue( "requested method missing", TS.isMethod( classUnderTest, "isEmpty",  boolean.class, null ));
            assertTrue( "requested method missing", TS.isMethod( classUnderTest, "reverse",  void.class,    null ));
            //
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "clear",    void.class,    null,                      Modifier.PUBLIC ));
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "fput",     void.class,    new Class[]{ Data.class }, Modifier.PUBLIC ));
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "lput",     void.class,    new Class[]{ Data.class }, Modifier.PUBLIC ));
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "fget",     Data.class,    null,                      Modifier.PUBLIC ));
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "lget",     Data.class,    null,                      Modifier.PUBLIC ));
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "isEmpty",  boolean.class, null,                      Modifier.PUBLIC ));
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "reverse",  void.class,    null,                      Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "List" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_List(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "List" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_List(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "List" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_List(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: List erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation__List(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testConstructorParameter = "test";                         // anything ;-)
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
           // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf gegebener Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_givenMethods_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            list.fput( new Data(1) );
            list.lput( new Data(2) );
            list.fput( new Data(3) );
            list.lput( new Data(3) );
            list.fget();
            list.lget();
            list.isEmpty();
            list.clear();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    

    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf reverse() (und zuvor lput(). */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_reverse_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            list.fput( new Data(1) );
            list.reverse();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Funktions-Test: "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_reverse_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = {
            new Data(3),
            new Data(2),
            new Data(1)
        };
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            for( int i=testDataArray.length;  --i>=0; )  assertEquals( testDataArray[i], list.lget() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()    
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[3];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[4];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);

        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[22];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[23];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no10(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[7];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no11(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[7];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.fput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.lget() );
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no12(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[8];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                // assertNotEquals( testDataArray[i], list.lget() );            // YATTB : BlueJ kann es nicht
                assertFalse( testDataArray[i].equals( list.lget() ));
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_reverse_no13(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[] testDataArray = new Data[8];
        for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.fput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                // assertNotEquals( testDataArray[i], list.lget() );            // YATTB : BlueJ kann es nicht
               assertFalse( testDataArray[i].equals( list.fget() ));
                i++;
            }//while
            assertEquals( testDataArray.length, i );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    //method()
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###
    
    /** Stress-Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_reverse_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[] testDataArray = new Data[3];
            for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( i, testDataArray.length );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Data[] testDataArray = new Data[1];
            for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( i, testDataArray.length );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_reverse_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[] testDataArray = new Data[3];
            for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( i, testDataArray.length );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Data[] testDataArray = {};
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            list.clear();
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( i, testDataArray.length );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: Funktion "reverse()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_reverse_no99(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "List";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[] testDataArray = new Data[3];
            for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
            list.reverse();
            int i=0;
            while( ! list.isEmpty() ){
                assertEquals( testDataArray[i], list.fget() );
                i++;
            }//while
            assertEquals( i, testDataArray.length );
        }catch( final TestSupportException | ArrayIndexOutOfBoundsException ex ){
            fail( ex.getMessage() );
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final List_I list = (List_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int noe=100;  --noe>=0; ){
                final Data[] testDataArray = new Data[noe];
                for( int i=testDataArray.length;  --i>=0; )  testDataArray[i] = new Data(i);
                if( noe%3 != 0 )  list.clear();                                 // sometimes clear (of empty list) and sometimes not
                try{
                    for( int i=testDataArray.length;  --i>=0; )  list.lput( testDataArray[i] );
                    list.reverse();
                    list.reverse();     // and again - now it's normal again
                    list.reverse();     // and again - now it's reverse again ;-)
                    int i=0;
                    while( ! list.isEmpty() ){
                        assertEquals( testDataArray[i], list.fget() );
                        i++;
                    }//while
                    assertEquals( i, testDataArray.length );
                }catch( final ArrayIndexOutOfBoundsException ex ){
                    fail( ex.getMessage() );
                }//try
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    // toString korrekt ?
    //method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   SUPPORT
    //###
    
    // NOTE: This method (itself) is unnecessary, since Strings are constants, but to keep code in common style - it is implemented.
    //...
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of [ms] for test => 4[s]
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 22;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A5;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
