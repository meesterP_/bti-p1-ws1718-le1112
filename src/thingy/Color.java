package thingy;


/**
 * LabExam1112_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Der enum Color beschreibt die moegliche Farbe eines Items.
 *<br />
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_1707xx_v00
 */
public enum Color {
    BLACK, BLUE, BROWN, CYAN, GOLD, GRAY, GREEN, MAGENTA, ORANGE, PINK, PURPLE, RED, TAN, VIOLET, WHITE, YELLOW;
}//enum
