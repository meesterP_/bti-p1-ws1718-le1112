package thingy;


/**
 * LabExam11112_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Der enum Size beschreibt die moegliche Geschwindigkeit eines Items.
 *<br />
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_1707xx_v00
 */
public enum Speed {
    SLOW, MEDIUM, FAST
}//enum
