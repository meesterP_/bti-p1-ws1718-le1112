package thingy;


/**
 * LabExam1112_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Die Klasse Item beschreibt ein Item.
 * Ein Item hat die folgenden Eigenschaften:
 * <ul>
 *     <li>Farbe (color) - hierauf kann mit getColor() zugegriffen werden.</li>
 *     <li>Geschwindigkeit (speed) - hierauf kann mit getSpeed() zugegriffen werden.</li>
 *     <li>Groesse (size) - hierauf kann mit getSize() zugegriffen werden.</li>
 *     <li>Wert (value) - hierauf kann mit getValue() zugegriffen werden.</li>
 * </ul>
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_1707xx_v00
 */
final public class Item {
    
    // Die Farbe eines Items
    final private Color color;
    
    // Die Geschwindigkeit eines Items
    final private Speed speed;
    
    // Die Groesse eines Items
    final private Size size;
    
    // Der Wert eines Items
    final private Long value;
    
    
    
    
    
    /**
     * Der Konstruktor erzeugt ein Item.
     *
     * @param color  bestimmt die Farbe des zu erzeugenden Items.
     * @param size   bestimmt die Groesse des zu erzeugenden Items.
     * @param speed  bestimmt die Geschwindigkeit des zu erzeugenden Items.
     * @param value  bestimmt den Wert des zu erzeugenden Items.
     */
    public Item(
        final Color color,
        final Size size,
        final Speed speed,
        final Long value
    ){
        this.color = color;
        this.size = size;
        this.speed = speed;
        this.value = value;
    }//constructor()
    
    
    
    
    
    @Override
    public boolean equals( final Object otherObject ){
        if( this == otherObject )  return true;
        if( null == otherObject )  return false;
        if( getClass()!=otherObject.getClass() )  return false;
        
        final Item other = (Item)( otherObject );
        if( size != other.size  )  return false;
        if( color != other.color  )  return false;
        if( speed != other.speed  )  return false;
        if( ! value.equals( other.value  ))  return false;          // NOTE: constants only exist once -> hence "!=" is ok ;-)
        return true;
    }//method()
    
    @Override
    public int hashCode(){
        final int prime = 31;
        int hashCode = ((null==size) ? 0 : size.hashCode());
        hashCode = prime*hashCode + ((null==color) ? 0 : color.hashCode());
        hashCode = prime*hashCode + ((null==speed) ? 0 : speed.hashCode());
        hashCode = prime*hashCode + ((null==value) ? 0 : value.hashCode());
        return  hashCode;
    }//method();
    
    @Override
    public String toString(){
        return String.format(
            "[<%s>: %s %s %s %s]",
            Item.class.getSimpleName(),
            color,
            speed,
            size,
            value
        );
    }//method()
    
    
    /**
     * getColor() liefert die Farbe des jeweiligen Items.
     */
    public Color getColor(){ return color; }
    
    /**
     * getSize() liefert die Groesse des jeweiligen Items.
     */
    public Size getSize(){ return size; }
    
    /**
     * getSpeed() liefert die Geschwindigkeit des jeweiligen Items.
     */
    public Speed getSpeed(){ return speed; }
    
    /**
     * getSize() liefert den Wert des jeweiligen Items.
     */
    public Long getValue(){ return value; }
    
}//class
