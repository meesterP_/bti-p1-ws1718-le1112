package a3;

public class Encoder implements Encoder_I {
    // Attributes
    private String text = "";
    private String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private String legalCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz., ";

    // Constructor
    public Encoder(String text) {
        assert text != null : "illegal parameter!";
        if (text != "") {
            boolean textLegal = true;
            boolean characterLegal = false;
            int characterLegalCounter = 1;
            for (int i = 0; i < text.length(); i++) {
                if (characterLegal) {
                    characterLegalCounter++;
                    characterLegal = false;
                }
                for (int j = 0; j < legalCharacters.length(); j++) {
                    if (text.charAt(i) == legalCharacters.charAt(j)) {
                        characterLegal = true;
                    }
                }
            }
            assert characterLegalCounter == text.length() : "Illegal Parameter!";       
        }
        this.text = text;
    }

    // Methods
    @Override
    public int getNumberOfBlanks() {
        int numberOfBlanks = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == ' ') {
                numberOfBlanks++;
            }
        }
        return numberOfBlanks;
    }

    @Override
    public int getNumberOfLetters() {
        int numberOfLetters = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c != ' ' && c != '.' && c != ',') {
                numberOfLetters++;
            }
        }
        return numberOfLetters;
    }

    @Override
    public String getOriginalText() {
        return text;
    }

    @Override
    public String encode(int code) {
        String newText = "";
        String textToLowercase = text.toLowerCase();
        int encodedCharPos = 0;
        for (int i = 0; i < textToLowercase.length(); i++) {
            char currentChar = textToLowercase.charAt(i);
            if (currentChar != ' ' && currentChar != '.' && currentChar != ',') {
                int j = 0;
                while (currentChar != alphabet.charAt(j)) {
                    j++;
                }

                encodedCharPos = j + code;
                while (encodedCharPos < 0) {
                    encodedCharPos += 26;
                }
                while (encodedCharPos > 25) {
                    encodedCharPos -= 26;
                }
                newText += alphabet.charAt(encodedCharPos);
            } else if (currentChar == ' ') {
                newText += 'X';
            } else if (currentChar == '.') {
                newText += 'T';
            } else {
                newText += 'Y';
            }
        }
        return newText;
    }

}
