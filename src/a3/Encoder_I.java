package a3;


/**
 * LabExam1112_4XIB1-P1    (PTP-BlueJ)<br />
 *<br />
 * Das Interface Encoder_I
 * <ul>
 *     <li>beschreibt einen Encoder, der Texte codiert und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und
 *         fordert die entsprechenden Methoden ein.
 *     </li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse Encoder muss
 * <ul>
 *     <li>einen oeffentlichen Konstruktor aufweisen,
 *         der der folgenden Signatur genuegt:<br />
 *         <code>Encoder( String )</code><br />
 *         <br />
 *         Der Konstruktor darf nur Texte akzeptieren,
 *         die aus dem modernen lateinischen Alphabet,
 *         Blanks, Komma und Punkt bestehen.
 *     </li>
 * </ul>
 *<br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse
 * findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/Prg_P1_LE_s17s_WI1_Proto.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_170710_v01
 */
public interface Encoder_I {
    
    /**
     * getNumberOfBlanks() liefert die Anzahl der im Original-Text enthaltenen Leerzeichen.
     * 
     * @return  Anzahl der im Original-Text enthaltenen Leerzeichen
     */
    int getNumberOfBlanks();
    
    /**
     * getNumberOfBlanks() liefert die Anzahl der im Original-Text enthaltenen Buchstaben.
     * 
     * @return  Anzahl der im Original-Text enthaltenen Buchstaben
     */
    int getNumberOfLetters();
    
    
    /**
     * getOriginalText() liefert den zuvor dem Konstruktor uebergebenen Original-Text.
     * 
     * @return  der Original-Text
     */
    String getOriginalText();
    
    
    /**
     * encode() codiert den Original-Text.
     * Dafuer muessen zunaechst die Buchstaben in Kleinbuchstaben gewandelt werden.
     * Danach sind Blanks durch "X", Kommata durch "Y" und Punkte durch "T" zu ersetzen.
     * Darauf folgend muss der als Parameter uebergebene Code zyklisch jeweils auf die
     * (nun Klein-)Buchstaben des Original-Textes addiert werden.
     *<br />
     * Alle Details (insbesondere zur Codierung) finden sich auf dem Aufgabenzetteln.
     * 
     * @return  der codierte Text
     */
    String encode( int code );
    
}//interface
