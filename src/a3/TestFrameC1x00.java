package a3;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1112_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben.
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann nicht als Beweis dienen, dass der Code fehlerfrei ist.
 * Es liegt in Ihrer Verantwortung sicher zu stellen, dass Sie fehlerfreien Code geschrieben haben.
 * Bei der Bewertung werden andere - konkret : modifizierte und haertere Tests - verwendet.
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_170710_v10
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A / "1e"
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet, wird aber IMMER mit angestartet). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "Encoder";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            final String testConstructorParameter = "test";                     // anything ;-)
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testConstructorParameter } ));
                TS.printDetailedInfoAboutObject( encoder, "encoder" );
                //
                if( TS.isActualMethod( encoder.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  encoder.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  encoder.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "Encoder". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_Encoder(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "Encoder". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_Encoder(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(                  classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isClassAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(           classUnderTest, "Encoder_I" ));
            //
            assertTrue( "requested constructor missing",    TS.isConstructor(                  classUnderTest, new Class<?>[]{ String.class } ));
            assertTrue( "false constructor access modifier",TS.isConstructorAccessModifierSet( classUnderTest, new Class<?>[]{ String.class }, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Encoder" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_Encoder(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(                  classUnderTest, "encode",             String.class, new Class[]{ int.class } ));
            assertTrue( "requested method missing",     TS.isMethod(                  classUnderTest, "getOriginalText",    String.class, null ));
            assertTrue( "requested method missing",     TS.isMethod(                  classUnderTest, "getNumberOfBlanks",  int.class,    null ));
            assertTrue( "requested method missing",     TS.isMethod(                  classUnderTest, "getNumberOfLetters", int.class,    null ));
            //
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "encode",             String.class, new Class[]{ int.class }, Modifier.PUBLIC ));   // -D interface ;-)
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "getOriginalText",    String.class, null,                     Modifier.PUBLIC ));   // -D interface ;-)
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "getNumberOfBlanks",  int.class,    null,                     Modifier.PUBLIC ));   // -D interface ;-)
            assertTrue( "false method access modifier", TS.isMethodAccessModifierSet( classUnderTest, "getNumberOfLetters", int.class,    null,                     Modifier.PUBLIC ));   // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Encoder" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_Encoder(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Encoder" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_Encoder(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Encoder" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_Encoder(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: Encoder erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation__Encoder(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testConstructorParameter = "test";                         // anything ;-)
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testConstructorParameter } ));
           // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "encode()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_encode_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testParameter = "test";                         // anything ;-)
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testParameter } ));
            encoder.encode(1);
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "getOriginalText()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_getOriginalText_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testParameter = "test";                         // anything ;-)
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testParameter } ));
            encoder.getOriginalText();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "getNumberOfBlanks()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_getNumberOfBlanks_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testParameter = "test";                         // anything ;-)
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testParameter } ));
            encoder.getNumberOfBlanks();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_getNumberOfLetters_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testParameter = "test";                         // anything ;-)
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testParameter } ));
            encoder.getNumberOfLetters();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Funktions-Test: "encode()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_encode_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "test";
        final String expectedResult = "uftu";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(1) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "encode()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_encode_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwuxyz";
        final String expectedResult = "abcdefghijklmnopqrstuvwuxyz";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(0) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getOriginalText()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getOriginalText_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "test";
        final String expectedResult = cloneIt( testText );
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getOriginalText() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getNumberOfBlanks()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getNumberOfBlanks_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "hallo welt";
        final int expectedResult = 1;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfBlanks() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getNumberOfLetters_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "test";
        final int expectedResult = 4;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  "getOriginalText()"
    //
    
    /** Einfacher Test: Funktion "getOriginalText()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getOriginalText_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyz";
        final String expectedResult = cloneIt( testText );
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getOriginalText() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getOriginalText()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getOriginalText_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = cloneIt( testText );
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getOriginalText() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getOriginalText()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getOriginalText_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZAaAABbBBCcCCDdDDEeEEFfFFGgGGHhHHIiIIJjJJKkKKLlLLMmMMNnNNOoOOPpPPQqQQRrRRSsSSTtTTUuUUVvVVWwWWXxXXYyYYZzZZ";
        final String expectedResult = cloneIt( testText );
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getOriginalText() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getOriginalText()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getOriginalText_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "XX, xXx. ABC,qQQ.ZZZ....YYY,,,,LLL    CCC   ,,,...,,,...    ..    ,,    .   ";
        final String expectedResult = cloneIt( testText );
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getOriginalText() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getOriginalText()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getOriginalText_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "";
        final String expectedResult = cloneIt( testText );
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getOriginalText() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  "getNumberOfLetters()"
    //
    
    /** Einfacher Test: Funktion "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfLetters_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyz";
        final int expectedResult = 26;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfLetters_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final int expectedResult = 26;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfLetters_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZAaAABbBBCcCCDdDDEeEEFfFFGgGGHhHHIiIIJjJJKkKKLlLLMmMMNnNNOoOOPpPPQqQQRrRRSsSSTtTTUuUUVvVVWwWWXxXXYyYYZzZZ";
        final int expectedResult = 156;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfLetters_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "XX, xXx. ABC,qQQ.ZZZ....YYY,,,,LLL    CCC   ,,,...,,,...    ..    ,,    .   ";
        final int expectedResult = 23;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfLetters_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "X";
        final int expectedResult = 1;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfLetters_no6(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "   ,,,...,,,...    ..    ,,    .   ";
        final int expectedResult = 0;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfLetters()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfLetters_no7(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "";
        final int expectedResult = 0;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfLetters() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  "getNumberOfBlanks()"
    //
    
    /** Einfacher Test: Funktion "getNumberOfBlanks()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfBlanks_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = " ";
        final int expectedResult = 1;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfBlanks() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfBlanks()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfBlanks_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "  ";
        final int expectedResult = 2;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfBlanks() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfBlanks()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfBlanks_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "     ";
        final int expectedResult = 5;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfBlanks() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfBlanks()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfBlanks_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "XX, xXx. ABC,qQQ.ZZZ....YYY,,,,LLL    CCC   ,,,...,,,...    ..    ,,    .   ";
        final int expectedResult = 24;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfBlanks() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "getNumberOfBlanks()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getNumberOfBlanks_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "   , ,   ,.,.,   . .   ";
        final int expectedResult = 14;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.getNumberOfBlanks() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method(
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  "encode()"
    //
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo";
        final String expectedResult = "hallo";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(0) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo";
        final String expectedResult = "ibmmp";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(1) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo";
        final String expectedResult = "jcnnq";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(2) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo";
        final String expectedResult = "kdoor";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(3) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo";
        final String expectedResult = "lepps";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(4) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no6(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo Welt";
        final String expectedResult = "halloXwelt";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(0) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no7(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo Welt";
        final String expectedResult = "ibmmpXxfmu";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(1) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no8(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Hallo Welt.";
        final String expectedResult = "ibmmpXxfmuT";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(1) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no9(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Aua, das tut weh.";
        final String expectedResult = "auaYXdasXtutXwehT";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(0) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no10(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Aua, das tut weh.";
        final String expectedResult = "bvbYXebtXuvuXxfiT";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(1) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no11(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "Aua, das tut weh.";
        final String expectedResult = "cwcYXfcuXvwvXygjT";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(2) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no20(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(0) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no21(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "bcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyza";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(1) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no22(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "cdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzab";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(2) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no23(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "defghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabc";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(3) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no24(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "efghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(4) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no25(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "fghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcde";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(5) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no26(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "jklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghi";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(9) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no27(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "rstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopq";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(17) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no28(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "xyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvw";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(23) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no29(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(25) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no30(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(26) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no31(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String expectedResult = "zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(-1) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no40(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "a,b.c,d.e,f.g,h.i,j.k,l.m,n.o,p.q,r.s,t.u,v.w,x.y,z.A,B.C,D.E,F.G,H.I,J.K,L.M,N.O,P.Q,R.S,T.U,V.W,X.Y,Z";
        final String expectedResult = "zYaTbYcTdYeTfYgThYiTjYkTlYmTnYoTpYqTrYsTtYuTvYwTxYyTzYaTbYcTdYeTfYgThYiTjYkTlYmTnYoTpYqTrYsTtYuTvYwTxYy";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(25) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_encode_no41(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = ",,,...,.,.,X X  XX  X  Y Y  YY  Y  ...yYy...,,,...RQ";
        final String expectedResult = "YYYTTTYTYTYwXwXXwwXXwXXxXxXXxxXXxXXTTTxxxTTTYYYTTTqp";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedResult, encoder.encode(25) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    //  combined tests
    //
    
    /** Funktions-Test: combined. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_combined_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText =              ",,,...,.,.,X X  XX  X  T T  TT  T  ...tTt...,,,...ML....";
        final String expectedEncodedResult = "YYYTTTYTYTYbXbXXbbXXbXXxXxXXxxXXxXXTTTxxxTTTYYYTTTqpTTTT";
        final int expectedNOB = 14;
        final int expectedNOL = 15;
        final String originalText = cloneIt( testText );
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            assertEquals( expectedEncodedResult, encoder.encode(4) );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###
    
    /** Stress-Test: Encoder erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_Encoder_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final String guardTestConstructorParameter = "test";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ guardTestConstructorParameter } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final String testConstructorParameter = null;
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testConstructorParameter } ));
            fail( "undetected illegal argument   ->   null" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try

        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: Encoder erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_4s_objectCreation_Encoder_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        final String guardTestConstructorParameter = "test";
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ guardTestConstructorParameter } ));
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnopqrtsuvwxyzäbcd", testName );  // ä
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnöpqrtsuvwxyzobcd", testName );  // ö
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnöpqrtsüvwxyzubcd", testName );  // ü
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnopqrtsuvwxyzÄbcd", testName );  // Ä
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnÖpqrtsuvwxyzobcd", testName );  // Ö
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnöpqrtsÜvwxyzubcd", testName );  // Ü
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnöpqrtsuvwxyzßbcd", testName );  // ß
        //
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,;. qrtsuvwxyz", testName );  // ;
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,:. qrtsuvwxyz", testName );  // :
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,µ. qrtsuvwxyz", testName );  // µ
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,+. qrtsuvwxyz", testName );  // +
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,-. qrtsuvwxyz", testName );  // -
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,*. qrtsuvwxyz", testName );  // *
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,%. qrtsuvwxyz", testName );  // /
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,/. qrtsuvwxyz", testName );  // \
        subTestObjectCreation_Encoder( requestedRefTypeWithPath, "abcdefghijklmnop,\\. qrtsuvwxy", testName );  // \
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsTwoPoints ));
        }//if
    }//method()
    //
    private void subTestObjectCreation_Encoder(
        final String requestedRefTypeWithPath,
        final String testConstructorParameter,                                  // test paramter - number that shall be checked if it is a palindromic number
        final String testName                                                   // name of test method that is responsible fot the test
    ){
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testConstructorParameter } ));
            fail( String.format( "undetected illegal argument -> argument was : \"%s\"",  testConstructorParameter ));
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
    }//method()
    
    
    /** Einfacher Test: Funktion "encode()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_encode_no90(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Encoder";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testText = "    ,,,,....abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ....,,,,    ";
        final String expectedResultFront = "XXXXYYYYTTTT";
        final String expectedResultEnd = "TTTTYYYYXXXX";
        final StringBuilder rawMiddle = new StringBuilder( "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" ); 
        try{
            final Encoder_I encoder = (Encoder_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[]{ testText } ));
            int shift = -65598;                                                 // 26*2523=65598 > 65536=2^16
            String intMinValueShiftTestText;
            loop:
            while( true ){
                final String expectedEncodedResult = expectedResultFront + rawMiddle.toString() + expectedResultEnd;
                assertEquals( expectedEncodedResult, encoder.encode( shift ) );
                if( shift >= 65600 ){                                           // 65600 = 2+65598 = 26 * 2523 (s.a.)
                    intMinValueShiftTestText = expectedEncodedResult;
                    break loop;
                }//if
                final char movingChar = rawMiddle.charAt(0);
                rawMiddle.deleteCharAt( 0 );
                rawMiddle.append( movingChar );
                shift++;
            }//loop
            assertEquals( intMinValueShiftTestText,  encoder.encode( Integer.MIN_VALUE ) ); // INTEGER.MINVALUE % 26 = "+2" ( = -24)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    //##########################################################################
    //###
    //###   SUPPORT
    //###
    
    // NOTE: This method (itself) is unnecessary, since Strings are constants, but to keep code in common style - it is implemented.
    private static String cloneIt( final String original ){
        final StringBuilder sb = new StringBuilder( original );
        final String theClone = sb.toString();                          // IT'S A LIE - constants are kept only once ;-)
        return theClone;
    }//method()
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of [ms] for test => 4[s]
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 63;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A3;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
