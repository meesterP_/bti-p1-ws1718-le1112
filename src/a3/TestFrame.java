package a3;

public class TestFrame {

    public static void main(String[] args) {
        String text = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int code = 1;
        Encoder ec = new Encoder(text);
        System.out.println(ec.encode(code));
    }

}
