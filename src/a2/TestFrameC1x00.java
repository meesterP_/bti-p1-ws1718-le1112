package a2;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
//
import java.util.List;
import java.util.ArrayList;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1112_4XIB1-P1    (PTP-BlueJ)<br />
 * <br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben.
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann nicht als Beweis dienen, dass der Code fehlerfrei ist.
 * Es liegt in Ihrer Verantwortung sicher zu stellen, dass Sie fehlerfreien Code geschrieben haben.
 * Bei der Bewertung werden andere - konkret : modifizierte und haertere Tests - verwendet.
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_170710_v10
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A / "1e"
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet, wird aber IMMER mit angestartet). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );
            
            final String requestedRefTypeName = "ArrayProcessor";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            final Data[][] para1 = {{ new Data(11), new Data(12) },  { new Data(21), new Data(22) }};   // just something valid ;-)
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final Object arrayProcessor = TS.generateRequestedObject( requestedRefTypeWithPath,  null,  null );
                TS.printDetailedInfoAboutObject( arrayProcessor, "arrayProcessor" );
                //
                if( TS.isActualMethod( arrayProcessor.getClass(),  "crop",  Data[].class,  new Class[]{ Data[][].class } )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  arrayProcessor.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  arrayProcessor.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "ArrayProcessor". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_ArrayProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "ArrayProcessor". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_ArrayProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                 TS.isClass( classUnderTest ));                                                                              // objects are created for tests ;-)
            assertTrue( "false class access modifier",  TS.isClassAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ArrayProcessor" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_ArrayProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(                  classUnderTest,  "crop",  Data[].class,  new Class[]{ Data[][].class } ));
            //
            assertTrue( "false class access modifier",  TS.isMethodAccessModifierSet( classUnderTest,  "crop",  Data[].class,  new Class[]{ Data[][].class },  Modifier.PUBLIC ));
            assertTrue( "false class access modifier",  TS.isMethodAccessModifierSet( classUnderTest,  "crop",  Data[].class,  new Class[]{ Data[][].class },  Modifier.STATIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ArrayProcessor" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_ArrayProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ArrayProcessor" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_ArrayProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ArrayProcessor" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_ArrayProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "crop()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_crop_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] testMatrix = {{ new Data(101), new Data(102) },  { new Data(201), new Data(202) }};  // just something valid ;-)
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException | TestSupportException ex ){                                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_crop_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {{ new Data(101), new Data(102) },  { new Data(201), new Data(202) }};  // just something most simple and valid ;-)
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = originalMatrix;
        final Data[] expectedOffcut = {};
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    //--------------------------------------------------------------------------
    //
    //      "crop()"
    //
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = originalMatrix;
        final Data[] expectedOffcut = {};
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = originalMatrix;
        final Data[] expectedOffcut = {};
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101) },
            { new Data(201) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = originalMatrix;
        final Data[] expectedOffcut = {};
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104) },
            { new Data(201), new Data(202), new Data(203) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202), new Data(203) }
        };
        final Data[] expectedOffcut = {
            new Data(104)
            //
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202), new Data(203), new Data(204) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202), new Data(203) }
        };
        final Data[] expectedOffcut = {
            //
            new Data(204)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no10(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108), new Data(109) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206), new Data(207), new Data(208), new Data(209) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308), new Data(309) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406), new Data(407), new Data(408), new Data(409) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507), new Data(508), new Data(509) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = originalMatrix;
        final Data[] expectedOffcut = {};
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no11(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206), new Data(207), new Data(208), new Data(209) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406), new Data(407) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507), new Data(508), new Data(509) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506) }
        };
        final Data[] expectedOffcut = {
            //
            new Data(207), new Data(208), new Data(209),
            new Data(307), new Data(308),
            new Data(407),
            new Data(507), new Data(508), new Data(509)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no12(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406), new Data(407), new Data(408), new Data(409) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506) }
        };
        final Data[] expectedOffcut = {
            new Data(107), new Data(108),
            //
            new Data(307), new Data(308),
            new Data(407), new Data(408), new Data(409),
            new Data(507)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no13(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206), new Data(207) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406), new Data(407) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507), new Data(508), new Data(509) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506) }
        };
        final Data[] expectedOffcut = {
            new Data(107), new Data(108),
            new Data(207),
            //
            new Data(407),
            new Data(507), new Data(508), new Data(509)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no14(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206), new Data(207), new Data(208), new Data(209) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506) }
        };
        final Data[] expectedOffcut = {
            new Data(107), new Data(108),
            new Data(207), new Data(208), new Data(209),
            new Data(307), new Data(308),
            //
            new Data(507)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no15(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108), new Data(109) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206), new Data(207), new Data(208) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406), new Data(407), new Data(408), new Data(409) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506) }
        };
        final Data[] expectedOffcut = {
            new Data(107), new Data(108), new Data(109),
            new Data(207), new Data(208),
            new Data(307),
            new Data(407), new Data(408), new Data(409)
            //
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no16(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108), new Data(109) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206), new Data(207), new Data(208) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507), new Data(508) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506) }
        };
        final Data[] expectedOffcut = {
            new Data(107), new Data(108), new Data(109),
            new Data(207), new Data(208),
            new Data(307),
            //
            new Data(507), new Data(508)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no20(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101) },
            { new Data(201), new Data(202) },
            { new Data(301), new Data(302), new Data(303) },
            { new Data(401), new Data(402), new Data(403), new Data(404) },
            { new Data(501), new Data(502) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101) },
            { new Data(201) },
            { new Data(301) },
            { new Data(401) },
            { new Data(501) }
        };
        final Data[] expectedOffcut = {
            //
            new Data(202),
            new Data(302), new Data(303),
            new Data(402), new Data(403), new Data(404),
            new Data(502)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no21(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202) },
            { new Data(301), new Data(302), new Data(303), new Data(304) },
            { new Data(401), new Data(402) },
            { new Data(501), new Data(502), new Data(503) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102) },
            { new Data(201), new Data(202) },
            { new Data(301), new Data(302) },
            { new Data(401), new Data(402) },
            { new Data(501), new Data(502) }
        };
        final Data[] expectedOffcut = {
            new Data(103),
            //
            new Data(303), new Data(304),
            //
            new Data(503)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no22(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202), new Data(203), new Data(204) },
            { new Data(301) },
            { new Data(401), new Data(402) },
            { new Data(501), new Data(502), new Data(503) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101) },
            { new Data(201) },
            { new Data(301) },
            { new Data(401) },
            { new Data(501) }
        };
        final Data[] expectedOffcut = {
            new Data(102), new Data(103),
            new Data(202), new Data(203), new Data(204),
            //
            new Data(402),
            new Data(502), new Data(503)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no23(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104) },
            { new Data(201), new Data(202), new Data(203) },
            { new Data(301), new Data(302) },
            { new Data(401) },
            { new Data(501), new Data(502), new Data(503) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101) },
            { new Data(201) },
            { new Data(301) },
            { new Data(401) },
            { new Data(501) },
        };
        final Data[] expectedOffcut = {
            new Data(102), new Data(103), new Data(104),
            new Data(202), new Data(203),
            new Data(302),
            //
            new Data(502), new Data(503)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no24(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202), new Data(203), new Data(204) },
            { new Data(301), new Data(302) },
            { new Data(401), new Data(402), new Data(403), new Data(404) },
            { new Data(501) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101) },
            { new Data(201) },
            { new Data(301) },
            { new Data(401) },
            { new Data(501) }
        };
        final Data[] expectedOffcut = {
            new Data(102), new Data(103),
            new Data(202), new Data(203), new Data(204),
            new Data(302),
            new Data(402), new Data(403), new Data(404)
            //
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no25(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108), new Data(109) },
            { new Data(201), new Data(202) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308) },
            { new Data(401) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101) },
            { new Data(201) },
            { new Data(301) },
            { new Data(401) },
            { new Data(501) }
        };
        final Data[] expectedOffcut = {
            new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108), new Data(109),
            new Data(202),
            new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308),
            //
            new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no30(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107), new Data(108), new Data(109) },
            { new Data(201), new Data(202), new Data(203), new Data(204), new Data(205), new Data(206), new Data(207), new Data(208), new Data(209) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308), new Data(309) },
            { new Data(401), new Data(402), new Data(403), new Data(404), new Data(405), new Data(406), new Data(407), new Data(408), new Data(409) },
            { new Data(501), new Data(502), new Data(503), new Data(504), new Data(505), new Data(506), new Data(507), new Data(508), new Data(509) },
            { new Data(601), new Data(602), new Data(603), new Data(604), new Data(605), new Data(606), new Data(607), new Data(608), new Data(609) },
            { new Data(701), new Data(702), new Data(703), new Data(704), new Data(705), new Data(706), new Data(707), new Data(708), new Data(709) },
            { new Data(801), new Data(802), new Data(803), new Data(804), new Data(805), new Data(806), new Data(807), new Data(808), new Data(809) },
            { new Data(901), new Data(902), new Data(903), new Data(904), new Data(905), new Data(906), new Data(907), new Data(808), new Data(909) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = originalMatrix;
        final Data[] expectedOffcut = {};
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no31(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103), new Data(104), new Data(105), new Data(106), new Data(107) },
            { new Data(201), new Data(202), new Data(203), new Data(204) },
            { new Data(301), new Data(302), new Data(303), new Data(304), new Data(305), new Data(306), new Data(307), new Data(308) },
            { new Data(401), new Data(402), new Data(403), new Data(404) },
            { new Data(501), new Data(502), new Data(503), new Data(504) },
            { new Data(601), new Data(602), new Data(603), new Data(604), new Data(605), new Data(606), new Data(607), new Data(608), new Data(609) },
            { new Data(701), new Data(702), new Data(703), new Data(704), new Data(705), new Data(706), new Data(707) },
            { new Data(801), new Data(802), new Data(803) },
            { new Data(901), new Data(902), new Data(903), new Data(904) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202), new Data(203) },
            { new Data(301), new Data(302), new Data(303) },
            { new Data(401), new Data(402), new Data(403) },
            { new Data(501), new Data(502), new Data(503) },
            { new Data(601), new Data(602), new Data(603) },
            { new Data(701), new Data(702), new Data(703) },
            { new Data(801), new Data(802), new Data(803) },
            { new Data(901), new Data(902), new Data(903) }
        };
        final Data[] expectedOffcut = {
            new Data(104), new Data(105), new Data(106), new Data(107),
            new Data(204),
            new Data(304), new Data(305), new Data(306), new Data(307), new Data(308),
            new Data(404),
            new Data(504),
            new Data(604), new Data(605), new Data(606), new Data(607), new Data(608), new Data(609),
            new Data(704), new Data(705), new Data(706), new Data(707),
            //
            new Data(904)
        };
       try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no60(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201), new Data(202) },
            { new Data(301), new Data(302), new Data(303), new Data(304) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101), new Data(102) },
            { new Data(201), new Data(202) },
            { new Data(301), new Data(302) }
        };
        final Data[] expectedOffcut = {
            new Data(103),
            //
            new Data(303), new Data(304)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "crop()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_crop_no61(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            { new Data(201) },
            { new Data(301), new Data(302), new Data(303), new Data(304) },
            { new Data(401), new Data(402), new Data(403) },
            { new Data(501), new Data(502) }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { new Data(101) },
            { new Data(201) },
            { new Data(301) },
            { new Data(401) },
            { new Data(501) }
        };
        final Data[] expectedOffcut = {
            new Data(102), new Data(103),
            //
            new Data(302), new Data(303), new Data(304),
            new Data(402), new Data(403),
            new Data(502)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "4s"
    //###
    
    //exception: null usw.
    
    /** Stress-Test: parameter contains empty array(s) */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_crop_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data[][] originalMatrix = {
            { new Data(101), new Data(102), new Data(103) },
            {},
            { new Data(301), new Data(302), new Data(303) },
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            {},
            {},
            {}
        };
        final Data[] expectedOffcut = {
            new Data(101), new Data(102), new Data(103),
            //
            new Data(301), new Data(302), new Data(303)
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final ClassNotFoundException | TestSupportException ex ){                   // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Stress-Test: parameter is null */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameterContains_null_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[][] testMatrix = {
                { new Data(101), new Data(102) },
                { new Data(201), new Data(202) }
            };
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){                                           // there shall be NO exception - hence, handle all exceptions the same way
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final Data[][] testMatrix = null;
        boolean expectedExceptionOccured = false;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            fail( "undetected illegal argument -> null (somewhere) accepted" );
        }catch( final ClassNotFoundException ex ){
            fail( ex.getMessage() );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
            expectedExceptionOccured = true;
        }catch( final Exception ex ){   // -> ("erroneous") NullPointerException
            fail( ex.getMessage() );
        }//try
        assertTrue( expectedExceptionOccured );
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: parameter contains (some forbidden) null */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameterContains_null_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[][] testMatrix = {
                { new Data(101), new Data(102) },
                { new Data(201), new Data(202) }
            };
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){                                           // there shall be NO exception - hence, handle all exceptions the same way
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final Data[][] testMatrix = {
            null
        };
        boolean expectedExceptionOccured = false;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            fail( "undetected illegal argument -> null (somewhere) accepted" );
        }catch( final ClassNotFoundException ex ){
            fail( ex.getMessage() );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
            expectedExceptionOccured = true;
        }catch( final Exception ex ){   // -> ("erroneous") NullPointerException
            fail( ex.getMessage() );
        }//try
        assertTrue( expectedExceptionOccured );
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: parameter contains (some forbidden) null */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameterContains_null_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[][] testMatrix = {
                { new Data(101), new Data(102) },
                { new Data(201), new Data(202) }
            };
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            // NO crash yet => success ;-)
        }catch( final Exception ex ){                                           // there shall be NO exception - hence, handle all exceptions the same way
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final Data[][] testMatrix = {
            null,
            { new Data(201), new Data(202) },
            { new Data(301), new Data(302) }
        };
        boolean expectedExceptionOccured = false;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            fail( "undetected illegal argument -> null (somewhere) accepted" );
        }catch( final ClassNotFoundException ex ){
            fail( ex.getMessage() );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
            expectedExceptionOccured = true;
        }catch( final Exception ex ){   // -> ("erroneous") NullPointerException
            fail( ex.getMessage() );
        }//try
        assertTrue( expectedExceptionOccured );
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: parameter contains (some forbidden) null */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameterContains_null_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[][] testMatrix = {
                { new Data(101), new Data(102) },
                { new Data(201), new Data(202) }
            };
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){                                           // there shall be NO exception - hence, handle all exceptions the same way
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final Data[][] testMatrix = {
            { new Data(101), new Data(102) },
            null,
            { new Data(301), new Data(302) }
        };
        boolean expectedExceptionOccured = false;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            fail( "undetected illegal argument -> null (somewhere) accepted" );
        }catch( final ClassNotFoundException ex ){
            fail( ex.getMessage() );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
            expectedExceptionOccured = true;
        }catch( final Exception ex ){   // -> ("erroneous") NullPointerException
            fail( ex.getMessage() );
        }//try
        assertTrue( expectedExceptionOccured );
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: parameter contains (some forbidden) null */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameterContains_null_no5(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Data[][] testMatrix = {
                { new Data(101), new Data(102) },
                { new Data(201), new Data(202) }
            };
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){                                           // there shall be NO exception - hence, handle all exceptions the same way
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        final Data[][] testMatrix = {
            { new Data(101), new Data(102) },
            { new Data(201), new Data(202) },
            null
        };
        boolean expectedExceptionOccured = false;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            fail( "undetected illegal argument -> null (somewhere) accepted" );
        }catch( final ClassNotFoundException ex ){
            fail( ex.getMessage() );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
            expectedExceptionOccured = true;
        }catch( final Exception ex ){   // -> ("erroneous") NullPointerException
            fail( ex.getMessage() );
        }//try
        assertTrue( expectedExceptionOccured );
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Stress-Test: parameter contains (some allowed) null */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameterContains_null_no99(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final Data someData = new Data(666);
        final Data[][] originalMatrix = {
            { someData,      someData,      someData,      someData,      someData,      someData,      someData,      someData,      someData },
            { someData,      null,          someData,      null,          null,          null,          someData,      someData,      null },
            { null,          null,          null,          null,          null,          null,          null,          null,          null },
            { null,          null,          null,          null },
            { someData,      someData,      someData,      someData },
            { someData,      null,          null,          someData },
            { null,          null,          null,          null,          null,          null },
            { new Data(801), new Data(802), new Data(803), new Data(804), new Data(805), new Data(806), new Data(807) },
            { null,          null,          null,          null,          null },
            { someData,      null,          null,          null,          null,          someData },
            { null,          someData,      null,          null,          null,          someData,      null },
            { null,          null,          someData,      null,          someData,      null,          null },
            { null,          null,          null,          someData,      null,          null,          null,          null },
            { null,          someData,      someData,      someData,      someData,      someData,      null },
            { someData,      null,          someData,      someData,      someData,      someData },
            { someData,      someData,      null,          someData,      someData },
            { someData,      someData,      someData,      null },
            { null,          null,          null,          null,          null,          null }
        };
        final Data[][] testMatrix = (Data[][])( originalMatrix.clone() );
        final Data[][] expectedCroppedMatrix = {
            { someData,      someData,      someData,      someData },
            { someData,      null,          someData,      null },
            { null,          null,          null,          null },
            { null,          null,          null,          null },
            { someData,      someData,      someData,      someData },
            { someData,      null,          null,          someData },
            { null,          null,          null,          null },
            { new Data(801), new Data(802), new Data(803), new Data(804) },
            { null,          null,          null,          null },
            { someData,      null,          null,          null },
            { null,          someData,      null,          null },
            { null,          null,          someData,      null },
            { null,          null,          null,          someData },
            { null,          someData,      someData,      someData },
            { someData,      null,          someData,      someData },
            { someData,      someData,      null,          someData },
            { someData,      someData,      someData,      null },
            { null,          null,          null,          null }
        };
        final Data[] expectedOffcut = {
            someData,      someData,      someData,      someData,      someData,
            null,          null,          someData,      someData,      null,
            null,          null,          null,          null,          null,
            //
            //
            //
            null,          null,
            new Data(805), new Data(806), new Data(807),
            null,
            null,          someData,
            null,          someData,      null,
            someData,      null,          null,
            null,          null,          null,          null,
            someData,      someData,      null,
            someData,      someData,
            someData,
            //
            null,          null
        };
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final Data[] actualOffcut = (Data[])( TS.callStaticFunction( classUnderTest,  "crop",  new Class[]{ Data[][].class },  new Object[]{ testMatrix } ) );
            assertTrue( TS.containEqualContentOn1stLevel( expectedOffcut, actualOffcut ));  // offcut result correct ?
            assertArrayEquals( expectedCroppedMatrix, testMatrix );                         // cropped matrix correct ?
        }catch( final Exception ex ){                                                       // there shall be NO exception - hence, handle all exceptions the same way
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 38;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A2;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
