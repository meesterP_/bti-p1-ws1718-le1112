package a2;

import java.util.ArrayList;

public class ArrayProcessor {
    // Attributes
    private static int firstDimensionLength = 0;
    private static int secondDimensionMinLength = Integer.MAX_VALUE;

    // Methods
    public static Data[] crop(Data[][] parameterMatrix) {
        assert parameterMatrix != null && parameterMatrix[0] != null : "Illegal parameter!";
        // if (arrayToCrop == null || arrayToCrop[0] == null) return null;

        firstDimensionLength = parameterMatrix.length;
        ArrayList<Data> croppedData = new ArrayList<>();

        // find out minimum amount of valid columns
        for (Data[] elem : parameterMatrix) {
            if (elem.length < secondDimensionMinLength) {
                secondDimensionMinLength = elem.length;
            }
        }

        // create the cropped matrix
        Data[][] croppedMatrix = new Data[firstDimensionLength][];
        for (int i = 0; i < croppedMatrix.length; i++) {
            Data[] currentArray = new Data[secondDimensionMinLength];
            croppedMatrix[i] = currentArray;
        }

        // fill the cropped matrix
        for (int i = 0; i < firstDimensionLength; i++) {
            for (int j = 0; j < secondDimensionMinLength; j++) {
                croppedMatrix[i][j] = parameterMatrix[i][j];
            }
        }

        // add the removed elements to the croppedData array and change the arrays of the parameter Matrix
        for (int i = 0; i < parameterMatrix.length; i++) {
            if (parameterMatrix[i].length > secondDimensionMinLength) {
                for (int j = secondDimensionMinLength; j < parameterMatrix[i].length; j++) {
                    croppedData.add(parameterMatrix[i][j]);
                }
                Data[] newArray = new Data[secondDimensionMinLength];
                for (int j = 0; j < secondDimensionMinLength; j++) {
                    newArray[j] = parameterMatrix[i][j];
                }
                parameterMatrix[i] = newArray;
            }
        }

        Data[] cutArray = new Data[croppedData.size()];
        croppedData.toArray(cutArray);
        return cutArray;
    }
}
