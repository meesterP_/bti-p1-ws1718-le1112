package a2;

public class TestFrame {
    public static void main(String[] args) {
        final Data[][] para1 = {{ new Data(11), new Data(12) },  { new Data(21), new Data(22) }};
        final Data[][] para2 = {{ new Data(11), new Data(12) },  { new Data(21), new Data(22), new Data(23) }};
        System.out.println(ArrayProcessor.crop(para1));
    }
}
