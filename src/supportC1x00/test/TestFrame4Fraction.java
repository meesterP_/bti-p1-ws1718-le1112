package supportC1x00.test;


import java.math.BigInteger;
//
//import supportC1x00.Fraction;


/**
 * LabExam P1<br />
 * <br />
 * ...
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExamDemo_4P1_162v02_170101_v03 (=LabExamDemo_4P1_162v02_161231_v03)
 */
public class TestFrame4Fraction {
    /*
    static public void main(String... unused ){
        Fraction f1,f2,f3,f4,f5,f6,f7,f8,f9,fa,fb,fc,fd,fe,ff,fg,fh,fi,fj,fk,fl,fm,fn,fs;
        
        f1 = new Fraction( 1L, 2L );
        f2 = new Fraction( 1L, 3L );
        f3 = (Fraction)( f1.clone() );
        f3.add( f2 );
        System.out.printf( "%s\n",  f1 );
        System.out.printf( "%s\n",  f2 );
        System.out.printf( "%s\n",  f3 );
        System.out.printf( "%6.2f%%\n",  f3.toPercentagePoint() );
        System.out.printf( "\n\n" );
        
        f2 = new Fraction( 1L, 6L );
        f3.add( f2 );
        System.out.printf( "%s\n",  f1 );
        System.out.printf( "%s\n",  f2 );
        System.out.printf( "%s\n",  f3 );
        System.out.printf( "%6.2f%%\n",  f3.toPercentagePoint() );
        System.out.printf( "\n\n" );
        
        f1 = new Fraction( 0L, 100L );
        f2 = new Fraction( 0L, 99991L );
        f3.add( f2 );
        System.out.printf( "%s\n",  f1 );
        System.out.printf( "%s\n",  f2 );
        System.out.printf( "%s\n",  f3 );
        System.out.printf( "%6.2f%%\n",  f3.toPercentagePoint() );
        System.out.printf( "\n\n\n\n" );
        
        
        f1 = new Fraction(  3L, 13L );
        f2 = new Fraction( 11L, 19L );
        f3 = (Fraction)( f1.clone() );
        f3.add( f2 );
        System.out.printf( "%s\n",  f1 );
        System.out.printf( "%s\n",  f2 );
        System.out.printf( "%s\n",  f3 );
        System.out.printf( "%6.2f%%\n",  f3.toPercentagePoint() );
        System.out.printf( "\n\n\n\n" );
        
        
        f1 = new Fraction( 11L, 101L );
        f2 = new Fraction( 13L, 103L );
        f3 = new Fraction( 17L, 107L );
        f4 = new Fraction( 19L, 109L );
        f5 = new Fraction( 21L, 113L );
        f6 = new Fraction( 23L, 127L );
        f7 = new Fraction( 29L, 131L );
        f8 = new Fraction( 31L, 137L );
        //
        f9 = (Fraction)( f1.clone() );
        f9.add( f2 );
        f9.add( f3 );
        f9.add( f4 );
        f9.add( f5 );
        f9.add( f6 );
        f9.add( f7 );
        f9.add( f8 );
        System.out.printf( "%s\n",  f1 );
        System.out.printf( "%s\n",  f2 );
        System.out.printf( "%s\n",  f3 );
        System.out.printf( "%s\n",  f4 );
        System.out.printf( "%s\n",  f5 );
        System.out.printf( "%s\n",  f6 );
        System.out.printf( "%s\n",  f7 );
        System.out.printf( "%s\n",  f8 );
        System.out.printf( "%s\n",  f9 );
        System.out.printf( "%6.2f%%\n",  f9.toPercentagePoint() );
        System.out.printf( "\n\n\n\n" );
        
        
        
        f1 = new Fraction( 11L, 211L );  final double d1 = 11.0/211.0;
        f2 = new Fraction( 13L, 223L );  final double d2 = 13.0/223.0;
        f3 = new Fraction( 17L, 227L );  final double d3 = 17.0/227.0;
        f4 = new Fraction( 19L, 229L );  final double d4 = 19.0/229.0;
        f5 = new Fraction( 21L, 233L );  final double d5 = 21.0/233.0;
        f6 = new Fraction( 23L, 239L );  final double d6 = 23.0/239.0;
        f7 = new Fraction( 29L, 241L );  final double d7 = 29.0/241.0;
        f8 = new Fraction( 31L, 251L );  final double d8 = 31.0/251.0;
        f9 = new Fraction( 37L, 257L );  final double d9 = 37.0/257.0;
        fa = new Fraction( 41L, 263L );  final double da = 41.0/263.0;
        fb = new Fraction( 43L, 269L );  final double db = 43.0/269.0;
        fc = new Fraction( 47L, 271L );  final double dc = 47.0/271.0;
        fd = new Fraction( 53L, 277L );  final double dd = 53.0/277.0;
        fe = new Fraction( 59L, 281L );  final double de = 59.0/281.0;
        ff = new Fraction( 61L, 283L );  final double df = 61.0/283.0;
        fg = new Fraction( 67L, 293L );  final double dg = 67.0/293.0;
        fh = new Fraction( 71L, 307L );  final double dh = 71.0/307.0;
        fi = new Fraction( 73L, 311L );  final double di = 73.0/311.0;
        fj = new Fraction( 79L, 313L );  final double dj = 79.0/313.0;
        fk = new Fraction( 83L, 317L );  final double dk = 83.0/317.0;
        fl = new Fraction( 89L, 331L );  final double dl = 89.0/331.0;
        fm = new Fraction( 97L, 337L );  final double dm = 97.0/337.0;
        fn = new Fraction(  0L, 347L );  final double dn =  0.0/347.0;
        //
        fs = (Fraction)( f1.clone() );
        fs.add( f2 );
        fs.add( f3 );
        fs.add( f4 );
        fs.add( f5 );
        fs.add( f6 );
        fs.add( f7 );
        fs.add( f8 );
        fs.add( f9 );
        fs.add( fa );
        fs.add( fb );
        fs.add( fc );
        fs.add( fd );
        fs.add( fe );
        fs.add( ff );
        fs.add( fg );
        fs.add( fh );
        fs.add( fi );
        fs.add( fj );
        fs.add( fk );
        fs.add( fl );
        fs.add( fm );
        fs.add( fn );
        System.out.printf( "%s\n",  f1 );
        System.out.printf( "%s\n",  f2 );
        System.out.printf( "%s\n",  f3 );
        System.out.printf( "%s\n",  f4 );
        System.out.printf( "%s\n",  f5 );
        System.out.printf( "%s\n",  f6 );
        System.out.printf( "%s\n",  f7 );
        System.out.printf( "%s\n",  f8 );
        System.out.printf( "%s\n",  f9 );
        System.out.printf( "%s\n",  fa );
        System.out.printf( "%s\n",  fb );
        System.out.printf( "%s\n",  fc );
        System.out.printf( "%s\n",  fd );
        System.out.printf( "%s\n",  fe );
        System.out.printf( "%s\n",  ff );
        System.out.printf( "%s\n",  fg );
        System.out.printf( "%s\n",  fh );
        System.out.printf( "%s\n",  fi );
        System.out.printf( "%s\n",  fj );
        System.out.printf( "%s\n",  fk );
        System.out.printf( "%s\n",  fl );
        System.out.printf( "%s\n",  fm );
        System.out.printf( "%s\n",  fn );
        System.out.printf( "%s\n",  fs );
        System.out.printf( "%6.2f%%\n",  fs.toPercentagePoint() );
        System.out.printf( "%6.2f%%\n",  ( d1+d2+d3+d4+d5+d6+d7+d8+d9+da+db+dc+dd+de+df+dg+dh+di+dj+dk+dl+dm+dn )*100.0 );
        System.out.printf( "\n\n" );
        //                                                                //---v-----v-----v-----
        final BigInteger dividend = fs.nominator.multiply( new BigInteger( "100000000000000000000" ));    // *100 *1.000.000.000.000.000.000
        final BigInteger divisor = fs.denominator;
        final BigInteger quotient = dividend.divide( divisor );
        final Double percentagePointx = Double.valueOf( quotient.toString() );
        final Double percentagePoint = percentagePointx  / 1__000_000__000_000__000_000.0;
        System.out.printf( "%6.32f%%\n",  percentagePoint );
        System.out.printf( "%6.32f%%\n",  ( d1+d2+d3+d4+d5+d6+d7+d8+d9+da+db+dc+dd+de+df+dg+dh+di+dj+dk+dl+dm+dn )*100.0 );
        System.out.printf( "\n\n\n\n" );
        
        
        
        f1 = new Fraction( 149L,  100_00__000000L );
        f2 = new Fraction( 100L,  100_00__000000L );
        f3 = new Fraction(  50L,  100_00__000000L );
        f4 = new Fraction(  49L,  100_00__000000L );
        f5 = new Fraction(   1L,  100_00__000000L );
        f6 = new Fraction(   0L,  100_00__000000L );
        f7 = new Fraction(  -1L,  100_00__000000L );
        f8 = new Fraction( -49L,  100_00__000000L );
        f9 = new Fraction( -50L,  100_00__000000L );
        fa = new Fraction(-100L,  100_00__000000L );
        fb = new Fraction(-149L,  100_00__000000L );
        System.out.printf( "%.6f%%\n",  f1.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f2.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f3.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f4.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f5.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f6.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f7.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f8.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f9.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  fa.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  fb.toPercentagePoint() );
        System.out.printf( "\n\n\n\n" );
        
        
        
        /##*
        f1 = Fraction.zero;
        f2 = Fraction.one;
        f3 = Fraction.oneQuarter;
        f4 = Fraction.oneHundred;
        f5 = Fraction.add( Fraction.zero, Fraction.one );
        f6 = Fraction.mul( Fraction.zero, Fraction.one );
        f7 = Fraction.mul( Fraction.one, Fraction.one );
        f8 = Fraction.mul( Fraction.oneQuarter, Fraction.oneHundred );
        System.out.printf( "%.6f%%\n",  f1.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f2.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f3.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f4.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f5.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f6.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f7.toPercentagePoint() );
        System.out.printf( "%.6f%%\n",  f8.toPercentagePoint() );
        *##/
    }//method()
    */
}//class
