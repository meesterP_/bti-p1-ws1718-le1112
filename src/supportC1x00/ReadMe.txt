###--------------------\\\vvv///--------------------
### CHECK-IN TOGGLE --->>> X <<<--- CHECK-IN TOGGLE
###--------------------///^^^\\\--------------------
################################################################################
###
###     C100 ::= Cycle 1.00
###     ===================
###



C100 is the "release" distributed to students.
Before this the cycle number is below 100 marking the development phase.
After lab examination the cycle number is above 100 marking the correction phase.

The cycle number neither marks a version nor a release (version)
it marks just a "step"
