package supportC1x00.underConstruction;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class TestJavaFX extends Application {
    
    @Override
    public void start( final Stage primaryStage ){
        
        final Label label = new Label( "Just a HELLO" );
        label.setFont( new Font( 100 ));
        
        final Group root = new Group( label );
        
        final Scene scene = new Scene( root );
        
        primaryStage.setScene( scene );
        primaryStage.setTitle( "Label Demo" );
        primaryStage.show();
        
    }//method()
    
}//class
