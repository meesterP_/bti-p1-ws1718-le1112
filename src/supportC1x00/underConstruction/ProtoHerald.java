package supportC1x00.underConstruction;


import javafx.application.Platform;
//
import javafx.embed.swing.JFXPanel;
//
import javafx.scene.control.Label;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Font;
//
import javafx.stage.Stage;


public class ProtoHerald {
    
    public static void proclaimViaFX( final String message ){
        stateContainer.iProclaimViaFX( message );
    }
    
    
    
    private void iProclaimViaFX( final String message ){
        // Initialises JavaFX:
        new JFXPanel();
        // Makes sure JavaFX doesn't exit when first window is closed:
        Platform.setImplicitExit(false);
        // Runs initialisation on the JavaFX thread:
        Platform.runLater( () -> initialiseGUI( message ) );
    }
    
    private void initialiseGUI( final String message ){
        Stage stage = new Stage();
        
        
        // ... more initialisation of the Stage
        final Label label = new Label( message );
        label.setFont( new Font( 100 ));
        
        final Group root = new Group( label );
        
        final Scene scene = new Scene( root );
        
        stage.setScene( scene );
        stage.setTitle( "ATTENTION:" );
        
        
        stage.show(); //stage.setVisible(true);
    }
    
    static final private ProtoHerald stateContainer = new ProtoHerald();
    
}//class
