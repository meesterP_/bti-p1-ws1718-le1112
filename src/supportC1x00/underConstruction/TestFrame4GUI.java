package supportC1x00.underConstruction;


public class TestFrame4GUI {
    
    public static void staticCall(){
        final String message = "THE MESSAGE";
        ProtoHerald.proclaimViaFX( message );
    }
    
    public static void main( final String... unused ){
    }//method()
    
    public void doTest( final String message ){
        ProtoHerald.proclaimViaFX( message );
    }//method()
    
}//class
