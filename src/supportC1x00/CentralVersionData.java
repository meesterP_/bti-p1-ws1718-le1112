package supportC1x00;


/**
 * Demo and Reference LabExam for P1<br />
 * <br />
 * Central "container" for version IDs.
 * Here the version IDs are "greped" from the classes there are defined in.
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/Prg_P1_LE_s17s_WI1_Proto.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_170710_v10                                                                     // ### <<<<====
 */
public class CentralVersionData {
    
    // lab exam version ID
    public final static String centralLabExamVersionID = "2017/07/10 P1 LabExam1112 [v1.10]";                           //  ### <<<<====
    
    
    // test support routine collection (the "engine" ;-) version ID  - generally the "Demo and Reference lab exam" shall contain the latest version
    public final static String centralTestSupportVersionID = "2017/06/14 v1.11";
    
    
    // version ID of WPI computer
    public final static String centralWPIComputerVersionID = TestResultAnalyzer.wpiComputerVersionID;
    
    
    // version ID of configuration class/structure/possibilities
    public final static String centralTestConfigurationVersionID = Configuration.configurationVersionID;
    
    
    
    // "serialVersionUID": test result data base (format for serialization) version ID   - generally the "Demo and Reference lab exam" shall contain the latest version
    public final static long centralTestResultDataBaseRelatedSerialVersionUID = 2017_06_14_0001L;
    
}//class
