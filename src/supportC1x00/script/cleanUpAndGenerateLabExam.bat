@echo off
ver
rem VCS: ...
rem ==============================================
echo.
echo "NOTE: This batch file has been developed & tested under Win10"
echo.
echo.
echo "----------------------------------------"
echo "You are/were here:"
echo "=================="
echo %~dp0
echo %CD%
echo "----------------------------------------"
cd ..\..
echo "Changing to:"
echo "============"
echo %CD%
echo "----------------------------------------"
echo.
echo "This batch file will generate the reference lab exam version"
echo "Hence, it will :"
echo "  o)  remove the solution(s)"
echo "  o)  remove the repository"
echo "  o)  remove the notes"
echo.
echo "Are you sure, you wonna do this?   Think carefully about it  for at least one second"
timeout  /t 1  /nobreak
pause
echo.
echo.
echo.
echo "Are you really sure, you wonna do this?   Think carefully about it  for at least two seconds"
timeout  /t 2  /nobreak
pause
echo.
echo.
echo.
echo "Are you absolutely sure, you wonna do this?   Think carefully about it  for at least three seconds"
timeout  /t 3  /nobreak
pause
echo.
echo.
echo.
echo "Are you -without any doubt- sure, you wonna do this?   Think carefully about it  for at least four seconds"
timeout  /t 4  /nobreak
pause
echo.
echo.
echo.
echo "Hmhm, looks like you are serious about that"
echo "I give you ten more seconds to reconsider"
echo "Afterwards, I will act as you told me - without questioning"
timeout  /t 10  /nobreak
echo.
rem ----------------------------------------------------------------------------
echo.
echo.
echo "Ok, your wish is my command ;-)"
echo "----------------------------------------"
echo %CD%\a1
if exist %CD%\a1 (
    del %CD%\a1\*.class
    del %CD%\a1\*.off
    del %CD%\a1\ExamineeInfo.*
    del %CD%\a1\ReadMe.txt
    del %CD%\a1\me.txt
    cd .  >%CD%\a1\me.txt
) else (
    echo %CD%\a1 does NOT exist
)
echo.
echo %CD%\a2
if exist %CD%\a2 (
    del %CD%\a2\*.class
    del %CD%\a2\*.off
    del %CD%\a2\ArrayProcessor.*
    del %CD%\a2\TestFrameSchmuddel.*
) else (
    echo %CD%\a2 does NOT exist
)
echo.
echo %CD%\a3
if exist %CD%\a3 (
    del %CD%\a3\*.class
    del %CD%\a3\*.off
    del %CD%\a3\Encoder.*
    del %CD%\a3\TestFrameSchmuddel.*
) else (
    echo %CD%\a3 does NOT exist
)
echo.
echo %CD%\a4
if exist %CD%\a4 (
    del %CD%\a4\*.class
    del %CD%\a4\*.off
    del %CD%\a4\CollectorA.*
    del %CD%\a4\CollectorB.*
    del %CD%\a4\CollectorC.*
    del %CD%\a4\CollectorC_Shf.*
    del %CD%\a4\CollectorC_IlonaBlanck.*
    del %CD%\a4\Help.*
) else (
    echo %CD%\a4 does NOT exist
)
echo.
echo %CD%\a5
if exist %CD%\a5 (
    del %CD%\a5\*.class
    del %CD%\a5\*.off
    del %CD%\a5\List.*
    del %CD%\a5\List_Shf.*
    del %CD%\a5\List_IlonaBlanck.*
) else (
    echo %CD%\a5 does NOT exist
)
echo.
echo %CD%\supportC1x00\test
if exist %CD%\supportC1x00\test (
    rmdir  /s  /q  %CD%\supportC1x00\test (
) else (
    echo %CD%\supportC1x00\test ( does NOT exist
)
echo.
echo %CD%\supportC1x00\trash
if exist %CD%\supportC1x00\trash (
    rmdir  /s  /q  %CD%\supportC1x00\trash (
) else (
    echo %CD%\supportC1x00\trash ( does NOT exist
)
echo.
echo %CD%\cards
if exist %CD%\cards (
    del %CD%\cards\*.class
) else (
    echo %CD%\cards does NOT exist
)
echo.
echo.
echo %CD%\TestResultDatabaseC1x00
if exist %CD%\TestResultDatabaseC1x00 (
    del %CD%\TestResultDatabaseC1x00\testResultDB.*
) else (
    echo %CD%\TestResultDatabaseC1x00 does NOT exist
)
echo.
rem ----------------------------------------------------------------------------
echo.
echo.
echo %CD%\_PROBLEMs_
if exist %CD%\_PROBLEMs_ (
    rmdir  /s  /q  %CD%\_PROBLEMs_
) else (
    echo %CD%\_PROBLEMs_ does NOT exist
)
echo.
echo %CD%\_NOTEs_
if exist %CD%\_NOTEs_ (
    rmdir  /s  /q  %CD%\_NOTEs_
) else (
    echo %CD%\NOTEs does NOT exist
)
echo.
rem ----------------------------------------------------------------------------
echo.
echo.
echo %CD%\supportC1x00\test
if exist %CD%\supportC1x00\test (
    rmdir  /s  /q  %CD%\ZZZ_support.off
) else (
    echo %CD%\supportC1x00\test does NOT exist
)
echo.
echo %CD%\supportC1x00\trash
if exist %CD%\supportC1x00\trash (
    rmdir  /s  /q  %CD%\supportC1x00\trash
) else (
    echo %CD%\supportC1x00\trash does NOT exist
)
echo.
echo %CD%\supportC1x00\hri
if exist %CD%\supportC1x00\hri (
    del %CD%\supportC1x00\hri\*.off
) else (
    echo %CD%\supportC1x00\hri does NOT exist
)
echo.
rem ----------------------------------------------------------------------------
echo.
echo.
echo %CD%\.git
if exist %CD%\.git (
    rmdir  /s  /q  %CD%\.git
) else (
    echo %CD%\.git does NOT exist
)
echo.
if exist %CD%\.gitignore (
    del %CD%\.gitignore
)
if exist %CD%\*.bash (
    del %CD%\*.bash
)
if exist %CD%\*.off (
    del %CD%\*.off
)
if exist %CD%\Bemerkung.txt (
    del %CD%\Bemerkung.txt
)
if exist %CD%\readme.* (
    del %CD%\readme.*
)
echo.
echo The job is done
pause
exit
