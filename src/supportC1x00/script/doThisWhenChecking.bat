@echo off
ver
rem VCS: ...
rem ==============================================
echo.
echo "NOTE: This batch file has been developed & tested under Win10"
echo.
echo.
echo "----------------------------------------"
echo "You are/were here:"
echo "=================="
echo %~dp0
echo %CD%
echo.
echo "DO ALL THE CHECKS LISTED IN THIS FILE"

pause
exit


###############################################################################
###
###     THE CHECKS
###

[ ] ist ausgelieferter Code in der richtigen Ansicht?
    TestFrameC*, Interface
    
[ ] stimmt Position der Fenster?
    
[ ] sind alle bzw. genau die richtigen Dateien geloescht worden?

[ ] TestFrameC* starten fuer jede(!) Aufgabe und
    dann im SourceCode des TestFrameC* kontrollieren
    Entspricht:   Testanzahl (#Runs)   numberOfTests  bzw.
    final static private int numberOfTests = ...;
    
[ ] TestFrameC* starten fuer jede(!) Aufgabe und
    dann im Terminal kontrollieren, ob
    jeweils 100% und WPI 100,00 erreicht werden?
    
[ ] Gibt es auch Tests fuer Dinge, die eingefordert werden?
    Insbesondere Verbotenes (z.B.: Parameter)
    
[ ] Tools -> Preferences -> Interface -> [X] Show unit testing tools
    
    