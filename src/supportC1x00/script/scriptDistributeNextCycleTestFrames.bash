#!/bin/bash
#-----------------------------------------------------------------------------
# Distribute Next evaluation Cycle TestFrame(s)

# identify script itself
#~~~~~~~~~~~~~~~~~~~~~~~
tmpScriptName=${0##*/}
echo ${tmpScriptName}



# Current Directory Path Position 
cdpp=${PWD}



vOld="C1x00"
vNew="C1x01"
if [ "${vOld}"  ==  "C1x00" ] && [ "${vNew}"  ==  "C1x00" ]; then
    printf "NOTE:\n"
    printf "C1x00 is defined as original lab exam version\n"
    printf "C1x00 is/was overwritten by Cx100 - you are aware of that?\n"
    printf "\n\n"
fi



refDirName="_sname_fname_XXX"
if [ ! -e ${refDirName} ]; then
    printf  "%s does NOT exist\n"  "${refDirName}"
    exit -1
fi

for dirName1 in * ;  do
    cnt=$( echo ${dirName1}  |  grep -P "__wi1ptp6[0-9][0-9]$" | wc -l )
    if [ 0 -lt ${cnt} ] ;  then
        cd  "${cdpp}/${dirName1}"
        #
        # save original support package (if not done yet)
        if [ -e "${cdpp}/${dirName1}/supportC1x00" ]; then
            #\=> supportC1x00 exists
            if [ ! -e "${cdpp}/${dirName1}/supportC1x00.exa.off" ]; then
                #\=> NO security backup of supportC1x00 yet => do it now ;-)
                mv  -v  "${cdpp}/${dirName1}/supportC1x00"  "${cdpp}/${dirName1}/supportC1x00.exa.off"
            fi
        fi
        #
        # remove old support package
        rm  -R  "${cdpp}/${dirName1}/support${vOld}"
        #
        # install new support package
        cp  -R  "${cdpp}/${refDirName}/support${vNew}"  "${cdpp}/${dirName1}"
        rm  "${cdpp}/${dirName1}/support${vNew}/test/"*.class
        rm  "${cdpp}/${dirName1}/support${vNew}/utility/"*.class
        #
        # update (TestFrames inside) exercises
        for dirName2 in "a1" "a2" "a3" "a4" "a5" ; do
            cd "${cdpp}/${dirName1}/${dirName2}"
            printf  "[%s]\n"  "${PWD}"
            #
            # save original TestFrame (if not done yet)
            if [ -e "${cdpp}/${dirName1}/${dirName2}/TestFrameC1x00.java" ]; then
                #\=> TestFrameC1x00.java exists
                if [ ! -e "${cdpp}/${dirName1}/${dirName2}/TestFrameC1x00.exa.off" ]; then
                    #\=> NO security backup of TestFrameC1x00.java yet => do it now ;-)
                    mv  -v  "${cdpp}/${dirName1}/${dirName2}/TestFrameX1x00.java"  "${cdpp}/${dirName1}/${dirName2}/TestFrameX1x00.java.exa.off"
                fi
            fi
            #
            # remove obsolete stuff
            rm  "${cdpp}/${dirName1}/${dirName2}/"*.class
            rm  "${cdpp}/${dirName1}/${dirName2}/"*.ctxt
            rm  "${cdpp}/${dirName1}/${dirName2}/TestFrame${vOld}.java"
            #
            # install new TestFrame
            cp  -v  "${cdpp}/${refDirName}/${dirName2}/TestFrame${vNew}.java"  "${cdpp}/${dirName1}/${dirName2}/TestFrame${vNew}.java"
        done
        printf  "\n\n"
    fi
done


exit 0

###
###  THE END
###  =======
###
#############################################################################




















#############################################################################
#############################################################################
#############################################################################
###
###  TRASH
###

# rm  -R  *wi1ptp*/supportC1x00.exa.off
