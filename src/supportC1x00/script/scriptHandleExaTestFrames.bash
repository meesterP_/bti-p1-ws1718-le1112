#!/bin/bash

# identify script itself
#~~~~~~~~~~~~~~~~~~~~~~~
tmpScriptName=${0##*/}
echo ${tmpScriptName}



# Current Directory Path Position 
cdpp=${PWD}


refDirName="_sname_fname_XXX"
if [ ! -e ${refDirName} ]; then
    printf  "%s does NOT exist\n"  "${refDirName}"
    exit -1
fi

for dirName1 in * ;  do
    cnt=$( echo ${dirName1}  |  grep -P "__wi1ptp6[0-9][0-9]$" | wc -l )
    if [ 0 -lt ${cnt} ] ;  then
        cd "${cdpp}/${dirName1}"
        mv  -v  support  support_exa
        for dirName2 in "a1" "a2" "a3" "a4" "a5" ; do
            cd "${cdpp}/${dirName1}/${dirName2}"
            printf  "[%s]\n"  "${PWD}"
            if [ ! -e "TestFrame.java.exa.off" ]; then
                mv  -v  "TestFrame.java"  "TestFrame.java.exa.off"
            fi
            cp  -v  "${cdpp}/${refDirName}/${dirName2}/TestFrame.java"  "TestFrame.java"
            diff  "TestFrame.java"  "TestFrame.java.exa.off"
            printf  "##############################################################################\n"
            printf  "####\n"
            printf  "####\n"
            printf  "\n\n"
        done
    fi
done


exit 0

###
###  THE END
###  =======
###
#############################################################################




















#############################################################################
#############################################################################
#############################################################################
###
###  TRASH
###
