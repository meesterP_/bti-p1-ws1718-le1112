@echo off
ver
rem VCS: ...
rem ==============================================
set supportDirectory=supportC1x00
set resultDBDirectory=TestResultDatabaseC1x00
echo.
echo %~dp0
echo %CD%
echo "----------------------------------------"
cd ..\..
echo %CD%
echo "----------------------------------------"
echo %CD%\a1
if exist %CD%\a1 (
    del %CD%\a1\*.class
) else (
    echo %CD%\a1 does NOT exist
)
echo.
echo %CD%\a2
if exist %CD%\a2 (
    del %CD%\a2\*.class
) else (
    echo %CD%\a2 does NOT exist
)
echo.
echo %CD%\a3
if exist %CD%\a3 (
    del %CD%\a3\*.class
) else (
    echo %CD%\a3 does NOT exist
)
echo.
echo %CD%\a4
if exist %CD%\a4 (
    del %CD%\a4\*.class
) else (
    echo %CD%\a4 does NOT exist
)
echo.
echo %CD%\a5
if exist %CD%\a5 (
    del %CD%\a5\*.class
) else (
    echo %CD%\a5 does NOT exist
)
echo.
rem ----------------------------------------------------------------------------
echo.
echo.
echo %CD%\cards
if exist %CD%\cards (
    del %CD%\cards\*.class
) else (
    echo %CD%\cards does NOT exist
)
echo.
rem ----------------------------------------------------------------------------
echo.
echo.
echo %CD%\thingy
if exist %CD%\thingy (
    del %CD%\thingy\*.class
) else (
    echo %CD%\thingy does NOT exist
)
echo.
rem ----------------------------------------------------------------------------
echo.
echo.
echo %CD%\%supportDirectory%
if exist %CD%\%supportDirectory% (
    del %CD%\%supportDirectory%\*.class
) else (
    echo %CD%\%supportDirectory% does NOT exist
)
echo.
echo %CD%\%resultDBDirectory%
if exist %CD%\%resultDBDirectory% (
    del %CD%\%resultDBDirectory%\testResultDB.*
) else (
    echo %CD%\%resultDBDirectory% does NOT exist
)
pause
exit
