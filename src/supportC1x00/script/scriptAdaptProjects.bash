#!/bin/bash
#----------------------------------------------------------------------------
# JOB:              adapt projects after LabExam
#----------------------------------------------------------------------------
# RESPONSIBLE:      Schaeferes, Michael [ShfM] ( email: Michael.Schafers@Hamburg-UAS.eu )
# KNOWN PROBLEMS:   none
# NOTEs:            none
# OPEN POINTS:      Should all be removed in the final version, anyway they have been
#                   marked with: _???_<YYMMDD>  (search/grep for _???_)
#----------------------------------------------------------------------------
#
# 16/01/30  v0.0.1  by ShfM
# 16/01/31  v1.0.0  by ShfM     1st draft
# 16/01/31  v1.0.1  by ShfM     actually used for 1st adaptation
# 16/01/31  v1.1.0  by ShfM     more service, but UNTESTED !
# 17/01/24  v1.1.1  by ShfM     using for lab exam s16w P1 WI1  and neglectable improvements:  ti1pt->wi1ptp ;  bluej315->bluej317 ; PTP-TI1->PTP-WI1
# 17/07/14  v1.1.2  by ShfM     using for lab exam s17s P1 WI1w and neglectable adaptations: different path again


# REMEMBER
# ========
#
# 1) "tmpLocalExpectedFolderName" und "tmpLocalRequestedFolderName" einstellen
# 2) "Alles" nach <tmpLocalExpectedFolderName> entpacken
# 3) Script adaptieren
# 4) Script starten





tmpLocalExpectedFolderName="_wi1ptp_Org"

tmpLocalRequestedFolderName="LabExamP1s17sAll"





# identify script itself
#~~~~~~~~~~~~~~~~~~~~~~~
tmpScriptName=${0##*/}
echo ${tmpScriptName}





exit                                                                            # GUARD ;-)
cd "/C/X/PRJ/JAVA/BlueJ/LabExam/s17s"
printf  "%s\n"  "${PWD}"





# check
if [ ! -d ${tmpLocalExpectedFolderName} ]; then
    printf  "%s is NO directory\n"  "${tmpLocalExpectedFolderName}"
fi
if [ -e ${tmpLocalRequestedFolderName} ]; then
    printf  "%s already exists"  "${tmpLocalRequestedFolderName}"
    exit -1
else
    printf  "[doing] mkdir  %s\n"   "${tmpLocalRequestedFolderName}"
    mkdir  "${tmpLocalRequestedFolderName}"
    returnCode=${?}
    if [ ${returnCode} -ne 0 ] ; then
        # problems have occurred
        echo -e "\e[01;31mmkdir error  ->  return code: ${returnCode}\e[00m"
        #    ^^! ---red---...........................................normal
        exit -1
    fi
    printf  "\n"
fi



# (empty!) list containing ...
ProblemList=
# OkList=
# ProblemListMeTxtMissing=
# ProblemListMeTxtFalse=
# ChkList=



# Current Directory Path Position 
cdpp=${PWD}





###-----------------------------------------------------------------------------
###
###     ACTION
###
# vvv___________________________________
# .../ti1pt###/win7/bluej315/PTP-TI1/...                                        # WS15/16
# .../wi1ptp###/win7/bluej317/PTP-WI1/...                                       # WS16/17
# .../wi1ptp###/PTP-WI1/...                                                     # SS17
cd "${tmpLocalExpectedFolderName}"
for dirName1 in * ;  do
    # ____vvvvvvvv__________________________
    # .../ti1pt###/win7/bluej315/PTP-WI1/...                                    # WS15/16
    # ../wi1ptp###/win7/bluej317/PTP-WI1/...                                    # WS16/17
    # ../wi1ptp###/PTP-WI1/...                                                  # SS17
    # check for expected ti1pt* folder and filter other stuff like synchronize script
    #
   #cnt=$( echo ${dirName1}  |  grep -P "^ti1pt\d\d\d$" | wc -l )               # WS15/16
   #cnt=$( echo ${dirName1}  |  grep -P "^wi1ptp\d\d\d$" | wc -l )              # WS16/17
    cnt=$( echo ${dirName1}  |  grep -P "^wi1ptp\d\d\d$" | wc -l )              # SS17
    fullDirName1="${cdpp}/${tmpLocalExpectedFolderName}/${dirName1}"
    currentDirName=${fullDirName1}
    if [ 0 -lt ${cnt} ] ;  then
        examineeName=""
        newFolderName=""
        errMark=0
        
        if [ ! -d ${currentDirName} ]; then
            printf  "MAIN ERROR: %s is NO directory\n"  "${currentDirName}"
            exit -1
        fi
        
        
        cd "${currentDirName}"
       #for dirName2 in * ;  do
       #    # _____________vvvv_____________________
       #    # .../ti1pt###/win7/bluej317/PTP-TI1/...                            # WS15/16
       #    # ../wi1ptp###/win7/bluej317/PTP-WI1/...                            # WS16/17
       #    #              ----                                                 # SS17
       #    # check for expected win7 folder
       #    cnt=$( echo ${dirName2}  |  grep -P "^win7$" | wc -l )
       #    fullDirName2="${fullDirName1}/${dirName2}"
       #    if [ 1 -ne ${cnt} ] ;  then
       #        printf  "unexpected file found: %s\n"  "${fullDirName2}"
       #        errMark=-1
       #        exit -1
       #    fi
       #    if [ ! -d ${fullDirName2} ]; then
       #        printf  "%s is NO directory\n"  "${fullDirName2}"
       #        errMark=-1
       #        exit -1
       #    fi
       #    
       #    
       #    cd "${fullDirName2}"
       #    for dirName3 in * ;  do
       #        # __________________vvvvvvvv____________
       #        # .../ti1pt###/win7/bluej317/PTP-TI1/...                        # WS15/16
       #        # ../wi1ptp###/win7/bluej317/PTP-WI1/...                        # WS16/17
       #        #              ----                                             # SS17
       #        # check for expected bluej317 folder
       #        cnt=$( echo ${dirName3}  |  grep -P "^bluej317$" | wc -l )
       #        fullDirName3="${fullDirName2}/${dirName3}"
       #        if [ 1 -ne ${cnt} ] ;  then
       #            printf  "unexpected file found: %s\n"  "${fullDirName3}"
       #            errMark=-1
       #            exit -1
       #        fi
       #        if [ ! -d ${fullDirName3} ]; then
       #            printf  "%s is NO directory\n"  "${fullDirName3}"
       #            errMark=-1
       #            exit -1
       #        fi
       #        
       #        
       #        cd "${fullDirName3}"
                for dirName4 in * ;  do
                    # ___________________________vvvvvvv____
                    # .../ti1pt###/win7/bluej317/PTP-TI1/...                # WS15/16
                    # ../wi1ptp###/win7/bluej317/PTP-WI1/...                # WS16/17
                    # ................/wi1ptp###/PTP-WI1/...                # SS17
                    
                    # check for expected PTP-WI1 folder
                    cnt=$( echo ${dirName4}  |  grep -P "^PTP-WI1$" | wc -l )
                   #fullDirName4="${fullDirName3}/${dirName4}"
                    fullDirName4="${currentDirName}/${dirName4}"
                    currentDirName=${fullDirName4}
                    if [ 1 -ne ${cnt} ] ;  then
                        printf  "unexpected file found: %s\n"  "${currentDirName}"
                        errMark=-1
                        exit -1
                    fi
                    if [ ! -d ${currentDirName} ]; then
                        printf  "%s is NO directory\n"  "${currentDirName}"
                        errMark=-1
                        exit -1
                    fi
                    
                    
                    
                    # now inside lab exam
                    #
                    cd "${currentDirName}"
                    for dirName5 in * ;  do
                        # check for expected a# folder
                        cnt=$( echo ${dirName5}  |  grep -P "^a[1-5]$" | wc -l )
                       #fullDirName5="${fullDirName4}/${dirName5}"
                        fullDirName5="${currentDirName}/${dirName5}"
                        exerciseDirName=${fullDirName5}
                        if [ 1 -ne ${cnt} ] ;  then
                            # TODO:
                            # \->   cards, package.bluej, supportC1x00, TestResultDatabaseC1x00, thingy
                            printf  "unexpected file found: %s\n"  "${exerciseDirName}"
                            
                        else
                            if [ ! -d ${exerciseDirName} ]; then
                                printf  "%s is NO directory\n"  "${exerciseDirName}"
                                errMark=+1
                                exit -1
                            fi
                            #
                            if [ ${dirName5} == "a1" ] ;  then
                                cd "${fullDirName5}"
                                if [ ! -e "me.txt" ]; then
                                    # printf  "%s/me.txt is missing\n"  "${dirName1}"
                                    errMark=+1
                                    if [ ${ProblemList} ] ; then
                                        ProblemList=("${ProblemList[@]}" "${dirName1}")
                                    else
                                        ProblemList=("${dirName1}")
                                    fi                                
                                else
                                    cnt=$( cat "me.txt"  |  grep -P "^[a-z]+_[a-z]+$" | wc -l )
                                    if [ 1 -eq ${cnt} ] ;  then
                                        examineeName="$( cat "me.txt" )"
                                        newFolderName="${examineeName}__${dirName1}"
                                    else
                                        errMark=+1
                                        if [ ${ProblemList} ] ; then
                                            ProblemList=("${ProblemList[@]}" "${dirName1}")
                                        else
                                            ProblemList=("${dirName1}")
                                        fi
                                    fi
                                fi
                            fi
                        fi
                    done
                done
       #    done
       #done
        
        if [ ${errMark} -eq 0 ] ;  then
            # everything seems to be fine
            printf  "\n"
            printf  "%s is ok  ->  %s\n"  "${dirName1}"  "${examineeName}"
            printf  "[doing] cp  -ir   %s   %s\n"  "${fullDirName4}"  "${cdpp}/${tmpLocalRequestedFolderName}/${newFolderName}"
            cp  -ir  "${fullDirName4}"  "${cdpp}/${tmpLocalRequestedFolderName}/${newFolderName}"
            returnCode=${?}
            if [ ${returnCode} -ne 0 ] ; then
                # problems have occurred
                echo -e "\e[01;31mcp error  ->  return code: ${returnCode}\e[00m"
                #    ^^! ---red---........................................normal
            fi
            printf  "\n"
        elif [ ${errMark} -eq 1 ] ;  then
            # name can NOT be computed
            printf  "\n"
            printf  "[doing] cp  -ir   %s   %s\n"  "${fullDirName4}"  "${cdpp}/${tmpLocalRequestedFolderName}/${dirName1}err"
            cp  -ir  "${fullDirName4}"  "${cdpp}/${tmpLocalRequestedFolderName}/${dirName1}err"
            returnCode=${?}
            if [ ${returnCode} -ne 0 ] ; then
                # problems have occurred
                echo -e "\e[01;31mcp error  ->  return code: ${returnCode}\e[00m"
                #    ^^! ---red---........................................normal
            fi
            printf  "\n"
        else
            #severe problems
            printf  "\n"
            echo -e "\e[01;31msevere problems with ${dirName1}\n\e[00m"
            #    ^^! ---red---..................................normal
            printf  "\n"
        fi
    else
        printf  "==>>>  %s  seen\n"  "${fullDirName1}"
    fi
done

if [ ${ProblemList} ] ; then
    echo -e "\e[01;31m#${#ProblemList[@]} PROBLEMs -> list of problematic folder:\n${ProblemList[@]}\n\e[00m"
    #    ^^! ---red---.................................................................................normal
    #
    # printf reagiert hier komisch
    #printf  "#%s PROBLEMs -> list of problematic folders:\n%s\n"  "${#ProblemList[@]}"  "${ProblemList[@]}"
fi



exit 0

###
###  THE END
###  =======
###
#############################################################################




















#############################################################################
#############################################################################
#############################################################################
###
###  TRASH
###
