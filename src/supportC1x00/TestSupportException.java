package supportC1x00;


/**
 * Demo and Reference LabExam for P1<br />
 * <br />
 * Test Support Exception
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/Prg_P1_LE_s17s_WI1_Proto.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1112_4XIB1-P1_171v01_170710_v10
 */
public class TestSupportException extends Exception {
    
    @Override
    public Throwable getCause(){
        return causingThrowable;
    }//method()
    
    
    
    /**
     * ...
     *
     * @param message    ...
     * @param throwable  ...
     */
    TestSupportException( final String message,  final Throwable throwable ){   // package scope on purpose
        super( message );
        causingThrowable = throwable;
    }//constructor()
    //
    /**
     * ...
     *
     * @param message    ...
     */
    TestSupportException( final String message ){
        this( message, new Throwable( "NO causing throwable existent - directly thrown : "+message ));
    }//constructor()
    //
    /**
     * ...
     */
    TestSupportException(){
        this( "test support function broken" );
    }//constructor()
    
    
    
    final private Throwable causingThrowable;
    
    // even so the test support exception is actually NOT test result data base related,  it gets the same ID for simplification
    final static private long serialVersionUID = CentralVersionData.centralTestResultDataBaseRelatedSerialVersionUID;
    
}//class
