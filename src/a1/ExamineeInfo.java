package a1;

public class ExamineeInfo implements ExamineeInfo_I {
    // Constructor
    public ExamineeInfo() {}

    // Methods
    @Override
    public String getExamineeSurName() {
        return "schwarz";
    }

    @Override
    public String getExamineeFirstName() {
        return "philipp";
    }

}
